/**
 *  @file
 *  @copyright defined in eos/LICENSE
 */
#pragma once

#define lemon  eosio
#define LEMON_DISPATCH  EOSIO_DISPATCH
/**
 * @defgroup c_api C API
 * @brief C++ API for writing ESIO Smart Contracts
 */

 /**
  * @defgroup cpp_api C++ API
  * @brief C++ API for writing ESIO Smart Contracts
  */
